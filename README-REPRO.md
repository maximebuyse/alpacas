Ecoop reproducibility package for Alpacas

## Lanching VSCODE

Change to the root project directory and lauch sbt

```
cd ~/Desktop/alpacas-master
sbt
```

inside the sbt console, type the following command to open VSCODE on the project

```
launchIDE
```

## Browsing the code

open the following files

- src/test/scala/tutorialpacas/RunningExample.scala
to view the code of the running example presented in Listing 1 of the paper

- src/test/scala/tutorialpacas/FlattenedGTS.scala
to view the code producing the flat SGTS encoding of the running example presented in Example 2 of the paper

- src/test/scala/tutorialpacas/DesignSpaceExploration.scala
to view the code of the design-space exploration case study presented in section 7 of the paper.

- src/test/scala/tutorialpacas/ExplorationExample.scala
to view the code of Listing 13, Section 7 of the paper

## Running the code

Inside sbt, the following commands allow to reproduce results presented in the paper

`test:runMain tutorialpacas.dse.DesignSpaceExploration`
reproduces the results presented in Table 3, Section 7

`test:runMain tutorialpacas.dse.ExplorationExample`
runs the code of Listing 13, Section 7

`test:runMain tutorialpacas.runningexample.FlattenedExample`
reproduces the flat SGTS compilation of the running example presented in Section 5, Example 2

`test:runMain tutorialpacas.runningexample.RunningExample`
runs the running example and reproduces the results presented in Section 6, Table 1, Table 2 of the paper.

`test`
runs the whole collection of ScalaTest test suites of alpacas.


## Running an interactive step simulation
`test:runMain tutorialpacas.runningexample.InteractiveSimulation`
launches an interactive step simulation of the running example

Once this example runs, you can manually select events to fire from a list and see the state updates it triggers. You can also backtrack the simulation by entering option -1.