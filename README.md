## ALPACAS: A Language for Parametric Assessment of Critical Architecture Safety

Original authors: 
Maxime Buyse, Rémi Delmas, Youssef Hamadi

For more information about Alpacas, a [research paper](https://drops.dagstuhl.de/opus/volltexte/2021/14048/) was published at ECOOP 2021, it features an overview of modeling features and analysis tools as well as various examples.

This project is a fork of https://github.com/uber-research/alpacas

The project is available under Uber's academic research open source licence, detailed in [`LICENSE`](https://gitlab.com/maximebuyse/alpacas/-/blob/master/LICENSE), which forbids any commercial use or derivative work. 

The software was designed and developed when all authors were employed at Uber's Advanced Technology Center Paris, France. 

### Description

Alpacas is a domain-specific language and algorithms aimed at architecture modeling and safety assessment for critical systems. It allows to study the effects of random and systematic faults on complex critical systems and their reliability. The underlying semantic framework of the language is Stochastic Guarded Transition Systems, for which Apacas provides a feature-rich declarative modeling language and algorithms for symbolic analysis and Monte-Carlo simulation, allowing to compute safety indicators such as minimal cutsets and reliability. Built as a domain-specific language deeply
embedded in Scala 3, Alpacas offers generic modeling capabilities and type-safety unparalleled in other existing safety assessment frameworks. This improved expressive power allows to address complex system modeling tasks, such as formalising the architectural design space of a critical function, and exploring it to identify the most reliable variants.

### How to cite
To cite this work, please use the following reference.

```
@InProceedings{buyse_et_al:LIPIcs.ECOOP.2021.5,
  author =	{Buyse, Maxime and Delmas, R\'{e}mi and Hamadi, Youssef},
  title =	{{ALPACAS: A Language for Parametric Assessment of Critical Architecture Safety}},
  booktitle =	{35th European Conference on Object-Oriented Programming (ECOOP 2021)},
  pages =	{5:1--5:29},
  series =	{Leibniz International Proceedings in Informatics (LIPIcs)},
  ISBN =	{978-3-95977-190-0},
  ISSN =	{1868-8969},
  year =	{2021},
  volume =	{194},
  editor =	{M{\o}ller, Anders and Sridharan, Manu},
  publisher =	{Schloss Dagstuhl -- Leibniz-Zentrum f{\"u}r Informatik},
  address =	{Dagstuhl, Germany},
  URL =		{https://drops.dagstuhl.de/opus/volltexte/2021/14048},
  URN =		{urn:nbn:de:0030-drops-140487},
  doi =		{10.4230/LIPIcs.ECOOP.2021.5},
  annote =	{Keywords: Domain-Specific Language, Deep Embedding, Scala 3, Architecture Modelling, Safety Assessment, Static Analysis, Monte-Carlo Methods}
}
```

### Usage

Alpacas is a scala 3 software library.

The best way to use it is to export a fat Jar of the library using:

```
sbt assembly
```

And add it as a dependency in your own scala 3 project in order to use its client API/DSL.

We might provide binary releases on maven mirrors at a later date.

The `src/test/scala/alpacas/tutorialpacas` contains example projects discussed in the research paper showing you how to get started using the DSL.
