val scala3Version = "3.0.0"

val alpacasVersion = "0.2.0"

lazy val root = project
  .in(file("."))
  .settings(
    name := "alpacas",
    version := alpacasVersion,
    scalaVersion := scala3Version,
    libraryDependencies += ("org.scala-graph" %% "graph-core" % "1.13.2").cross(CrossVersion.for3Use2_13),
    libraryDependencies += ("org.scalaz" %% "scalaz-core" % "7.3.3").cross(CrossVersion.for3Use2_13),
    libraryDependencies += "org.apache.commons" % "commons-math3" % "3.6",

    libraryDependencies += "com.lihaoyi" %% "sourcecode" % "0.2.7",
    libraryDependencies += ("org.scalatest" %% "scalatest-funsuite" % "3.2.9" % "test"),
    libraryDependencies += ("org.scalatest" %% "scalatest" % "3.2.9" % "test"),
    libraryDependencies += "org.scalacheck" %% "scalacheck" % "1.15.4" % "test",
    libraryDependencies += "org.typelevel" %% "cats-core" % "2.6.1",
    libraryDependencies += "org.scala-lang.modules" %% "scala-parallel-collections" % "1.0.3",

    assembly / assemblyJarName := s"fat-alpacas_$scala3Version-$alpacasVersion.jar",

    Compile/ run / fork := false, 
    Test/ run / fork := false, 
    Test / test / fork := false,
    Test / testForkedParallel := false,      
    javaOptions += "-Xmx6G",
  )

enablePlugins(JavaAppPackaging)
enablePlugins(GraalVMNativeImagePlugin)
