package alpacas.transition

import scala.collection.mutable.*
import org.apache.commons.math3.distribution.*

import java.util.UUID

import alpacas.ast.*
import alpacas.CausalModel
import alpacas.Event
import alpacas.InputDistribution
import alpacas.Policy


/** State assertion representation
  */
case class StateAssertion[T](l: Expr.Svar[T], r: Expr[T], line: Int) {
  override def toString = s"Assert(\n${l.toString(1)},\n${r.toString(1)})"
  def toString(m: CausalModel) = s"${l.toString(m)} := ${r.toString(m)}"
}

type StateAssertionBuilder = ArrayBuffer[StateAssertion[_]]
object StateAssertionBuilder {
  def apply(): StateAssertionBuilder = {
    ArrayBuffer()
  }
}

case class Transition(ev: Event, guard: Expr[Boolean], a: StateAssertionBuilder) 

/** Internal representation of event distribution */
type InternalDistribution = RealDistribution

object InternalDistribution {
  def apply(input: InputDistribution): InternalDistribution = {
    input match
      case InputDistribution.Exponential(lambda) => 
        assert(lambda > 0, "Parameter of exponential distribution must be > 0")
        ExponentialDistribution(1 / lambda) // math3 works with mean rather than lambda
      case InputDistribution.Dirac(d) => 
        assert(d >= 0, "Parameter of Dirac distribution must be >= 0")
        EnumeratedRealDistribution(Array(d))
      case InputDistribution.Weibull(shape, scale) =>
        assert(shape > 0, "Shape parameter of Weibull distribution must be > 0")
        assert(scale > 0, "Scale parameter of Weibull distribution must be > 0")
        WeibullDistribution(shape, scale)
      case InputDistribution.Uniform(a, b) =>
        assert(a >= 0, "Lower bound of uniform distribution must be >= 0")
        assert(a <= b, "Upper bound of uniform distribution must be >= to lower bound")
        UniformRealDistribution(a, b)
      case InputDistribution.UserDefined(d) => d
  }
}

object EventIds {
  opaque type EventId = UUID

  object EventId{
    def apply(): EventId = UUID.randomUUID()
  }
}

import EventIds.*

case class UndefinedDistribution() extends Error
