package alpacas.assertion

import scala.collection.mutable.ArrayBuffer

import alpacas.ast.*

type FlowAssertions = ArrayBuffer[FlowAssertion[_]]
object FlowAssertions {
  def apply(): FlowAssertions = {
    ArrayBuffer()
  }
}


case class FlowAssertionBuilder(assertions: FlowAssertions)

/** Flow assertion representation
  */
case class FlowAssertion[T](l: Expr.Fvar[T], r: Expr[T], line: Int) {
  override def toString = s"Assert(\n${l.toString(1)},\n${r.toString(1)})"
}