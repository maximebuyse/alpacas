package alpacas.errors

import cats.data.Validated.*
import cats.data.*

trait AlpacasError extends Error

type ValidationResult[A] = ValidatedNec[AlpacasError,A]
type EitherResult[A] = Either[NonEmptyChain[AlpacasError], A]
