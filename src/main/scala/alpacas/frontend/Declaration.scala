package alpacas.frontend

import scala.collection.mutable.ArrayBuffer
import sourcecode.*
import org.apache.commons.math3.distribution.*

import alpacas.transition.*
import alpacas.transition.EventIds.*
import alpacas.ast.*
import alpacas.ast.StateIds.*
import alpacas.ast.FlowIds.*
import alpacas.InputDistribution
import alpacas.Policy

trait VariablesFactory {

  /** Incoming flow variable declaration */
  object InFlow {
    def apply[T](using fvar: CompFVSet, name: sourcecode.Name, line: sourcecode.Line): Expr.Fvar[T] = {
      val res = new Expr.Fvar[T](FlowId(), name.value, line.value)
      fvar.in += res
      res
    }
  }

  /** Incoming flow variables vector declaration */
  object InFlows {
    def apply[T](n: Int)(using fvar: CompFVSet, name: sourcecode.Name, line: sourcecode.Line): Vector[Expr.Fvar[T]] = {
      Vector.tabulate(n){ i =>
        val res = new Expr.Fvar[T](FlowId(), name.value + s"($i)", line.value)
        fvar.in += res
        res
      }
    }
  }

  /** Outgoing flow variable declaration */
  object OutFlow {
    def apply[T](using fvar: CompFVSet, name: sourcecode.Name, line: sourcecode.Line): Expr.Fvar[T] = {
      val res = new Expr.Fvar[T](FlowId(), name.value, line.value)
      fvar.out += res
      res
    }
  }

  /** Outgoing flow variables vector declaration */
  object OutFlows {
    def apply[T](n: Int)(using cfvs: CompFVSet, name: sourcecode.Name, line: sourcecode.Line): Vector[Expr.Fvar[T]] = {
      Vector.tabulate(n){ i =>
        val res = new Expr.Fvar[T](FlowId(), name.value + s"($i)", line.value)
        cfvs.out += res
        res
      }
    }
  }

  /** State variable declaration */
  object State {
    def apply[T](init: T)(using svar: StateVarSet, name: sourcecode.Name, line: sourcecode.Line): Expr.Svar[T] = {
      val res = new Expr.Svar[T](StateId(), init, name.value, line.value)
      svar += res 
      res
    }
  }

  /** State variables vector declaration */
  object States {
    def apply[T](n: Int)(f: Int => T)(using svs: StateVarSet, name: sourcecode.Name, line: sourcecode.Line): Vector[Expr.Svar[T]] = {
      Vector.tabulate(n){i => 
        val res = new Expr.Svar[T](StateId(), f(i), name.value + s"($i)", line.value)
        svs += res
        res
      }
    }
    def apply[T](n: Int)(init: T)(using svs: StateVarSet, name: sourcecode.Name, line: sourcecode.Line): Vector[Expr.Svar[T]] = {
      States(n)(i => init)
    }
  }
}
