package alpacas

import scala.collection.mutable.ArrayBuffer
import org.apache.commons.math3.distribution.*

import alpacas.frontend.*
import alpacas.assertion.*
import alpacas.transition.*


object Modelling extends ComponentFactory 
  with VariablesFactory 
  with ExpressionFactory 
  with TransitionFactory

/** Extended by all component definitions */
class Component {
  given assert: FlowAssertionBuilder = FlowAssertionBuilder(FlowAssertions())
  given trans: TransitionBuilder = TransitionBuilder(Transitions())
  given fvar: CompFVSet = new CompFVSet()
  given svar: StateVarSet = new ArrayBuffer()
  given subcomps: Subcomponents = new ArrayBuffer()

  var name = ""
  var fullName = ""

  override def toString = {
    assert.assertions.mkString("AssertionBuilder :\n", ",\n", "\n") + 
    trans.transitions.mkString("Transitions :\n",",\n","\n") +
    fvar.in.mkString("In Flow variables: ", ", ", "\n") +
    fvar.out.mkString("Out Flow variables: ", ", ", "\n") +
    svar.mkString("State variables: ", ", ", "\n") +
    s"Number of Subcomponents: ${subcomps.size}"
  }
}

/** Input format for event probability distribution */
enum InputDistribution {
  case Exponential(lambda: Double)
  case Dirac(delta: Double)
  case Weibull(shape: Double, scale: Double)
  case Uniform(a: Double, b: Double)
  case UserDefined(d: RealDistribution)
}

enum Policy {
  case Memory
  case Restart
}


import EventIds.*

class Event(
  val id: EventId,
  val name: String,
  val distrib: Option[InternalDistribution],
  val weight: Option[Double],
  val policy: Policy,
  val urgent: Boolean,
){
  var hidden: Boolean = false
  def hide: Unit = hidden = true
  def sample(): Double = distrib match
    case Some(dis) => dis.sample()
    case None => throw UndefinedDistribution()
  override def toString = s"Event(${id}, $name)"
  override def equals(that: Any) = 
    that match
      case t: Event => t.id == this.id
      case _ => false
}

/** Event declaration */
object Event{
  def apply()(using name: sourcecode.Name): Event = {
    new Event(EventId(), name.value, None, None, Policy.Restart, false)
  }
  def apply(
    distrib: InputDistribution, 
    policy: Policy = Policy.Restart
  )(using name: sourcecode.Name): Event = {
    val internalDis = InternalDistribution(distrib)
    new Event(EventId(), name.value, Some(internalDis), None, policy, false)
  }
  def apply(
    distrib: InputDistribution, 
    weight: Double, 
  )(using name: sourcecode.Name): Event = {
    val internalDis = InternalDistribution(distrib)
    new Event(EventId(), name.value, Some(internalDis), Some(weight), Policy.Restart, false)
  }
  def apply(
    distrib: InputDistribution, 
    weight: Double, 
    policy: Policy
  )(using name: sourcecode.Name): Event = {
    val internalDis = InternalDistribution(distrib)
    new Event(EventId(), name.value, Some(internalDis), Some(weight), policy, false)
  }
  def urgent()(using name: sourcecode.Name): Event = {
    new Event(EventId(), name.value, None, None, Policy.Restart, true)
  }
}

import Modelling.{*, given}

/** Urgent delay (used to build causal feedback loops). */
class Delay[A: Lifted](init: A) extends Component {
  val in = InFlow[A]
  val out = OutFlow[A]
  val state = State[A](init = init)
  val propagate = Event.urgent() 
  transitions { When(propagate) If (!(in === state)) Then { state := in } }
  assertions { out := state }
}
