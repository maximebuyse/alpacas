package alpacas.step


import alpacas.causalModel.*
import alpacas.exprEval.*
import alpacas.state.*


/** @param state state variables assignement
  * @param flowDef flow assertions in topological order
  * @return completed variable assignement after flow propagation
  */
def varAssign(state: UpdateFlows, flowDef: FlowDefinition): StableState = {
  flowDef.foldLeft(state){case (state, fdef) => state.writeFV(fdef.l, eval(fdef.r, state))}.finishUpd()
}