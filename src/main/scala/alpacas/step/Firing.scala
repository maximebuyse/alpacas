package alpacas.step

import scala.collection.mutable.Queue
import cats.data.Validated.*
import cats.implicits.*
import scala.language.implicitConversions

import alpacas.ast.*
import alpacas.*
import alpacas.state.*
import alpacas.transition.*
import alpacas.transition.EventIds.*
import alpacas.CausalModel
import alpacas.exprEval.*
import alpacas.errors.*


/** @param m flattened model
  * @param state current variable assignement
  * @return list of fireable transitions
  */
def fireable(m: CausalModel, state: State): List[Event] = {
  val res = for {
    t <- m.allTransitions.toList
    if eval(t.guard, state) && !t.ev.hidden
  } yield t.ev
  res
} 

/** Fireable transitions set updated to current state */
def updateFireable(m: CausalModel, state: State, fireableTr: EventSet): Unit = {
  for (t <- m.allTransitions.toList) {
    if eval(t.guard, state) && !t.ev.hidden then
      fireableTr.add(t.ev.id) 
    else 
      fireableTr.remove(t.ev.id)
  }
}

/** @param m flattened model 
  * @param state current state
  * @param ev event to fire
  * @return new state after transition firing
  */
def fireOne(m: CausalModel, state: StableState, ev: EventId): StableState = {
  def updateStateVars(state: UpdateStates, t: Transition): UpdateStates = {
    t.a.foldLeft(state){case (state, StateAssertion(l, r, line)) => 
      state.writeSV(l, eval(r, state))
    }
  }
  val tr = m.allTransitions(m.idConverter.evIntIds(ev))

  if eval(tr.guard, state) then
    val updatedState = updateStateVars(state.startStatesUpd(), tr).startFlowsUpd()
    varAssign(updatedState, m.flowDef)
  else
    state // if the transition is not fireable, we return the unmodified state
}

def propagateUrgent(m: CausalModel, state: StableState, fireableUrg: EventSet): EitherResult[StableState] = {

  def successorsBFS(state: StableState): EitherResult[Set[StableState]] = {
    val queue = Queue(state)
    var res: Set[StableState] = Set()
    var explored = 0
    while (!queue.isEmpty && explored < 1000) {
      explored += 1
      val state = queue.dequeue()
      var stable = true
      m.allUrgentTransitions.foreach{t =>
        if eval(t.guard, state) then
          stable = false
          queue.enqueue(fireOne(m, state, t.ev.id))
      }
      if stable then
        res = res + state
    }
    if (explored < 1000) then
      Right(res)
    else 
      DivergingUrgentUnfolding(state.toString(m)).invalidNec.toEither
  }

  successorsBFS(state).flatMap{nextStates =>
    if nextStates.size == 1 then
      Right(nextStates.head)
    else 
      DivergingUrgentUnfolding(state.toString(m)).invalidNec.toEither
  }
}

def fire(m: CausalModel, state: StableState, ev: EventId, fireableUrg: EventSet): EitherResult[StableState] = {
  val newState = fireOne(m, state, ev)
  propagateUrgent(m, newState, fireableUrg)
}
