package alpacas.step

import alpacas.errors.*

sealed trait StepError extends AlpacasError

case class UnspecifiedExpectation(evname: String) extends StepError {
  override def toString = 
    s"Conflict on event $evname with unspecified weight"
}

case class DivergingUrgentUnfolding(startingPoint: String) extends StepError
