package alpacas.mcs

import alpacas.errors.*

sealed trait McsError extends AlpacasError

case class UnspecifiedDistribution(evname: String) extends McsError {
  override def toString = 
    s"The Distribution of event $evname must be specified"
}
