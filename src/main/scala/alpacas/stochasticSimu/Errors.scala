package alpacas.stochasticSimu

import alpacas.errors.*

sealed trait StochasticSimulationError extends AlpacasError

case class LiveLock() extends StochasticSimulationError 
