package alpacas.stochasticSimu

import scala.collection.parallel.CollectionConverters.*
import cats.{Applicative, Traverse}
import cats.data.Validated.*
import cats.implicits.*
import org.apache.commons.math3.distribution.*
import scala.language.implicitConversions

import alpacas.*
import alpacas.causalModel.*
import alpacas.frontend.*
import alpacas.ast.*
import alpacas.state.*
import alpacas.step.*
import alpacas.exprEval.*
import alpacas.flattening.*
import alpacas.errors.*


trait IndicatorsSimu {

  def confidenceInterval(l: Vector[Double], confidence: Double, nbSimulations: Double): (Double, Double) = {
    val average = l.reduce(_ + _) / nbSimulations
    val stdev = l.map(v => math.abs(v - average)).reduce(_ + _) / nbSimulations
    val q = NormalDistribution().inverseCumulativeProbability((1 + confidence) / 2)
    (average - stdev * q / math.sqrt(nbSimulations), average + stdev * q / math.sqrt(nbSimulations))
  }
    
  /** Stochastic simulation for Mean Time To Failure estimation
    * Potential errors are printed and stop simulation 
    * @param m top-level component
    * @param condition failure condition
    * @param ndSimulations number of simulations to perform
    * @param batchNumber number of batches of simulations (executed in parallel)
    * @param confidence confidence level (default 0.95)
    * @return lower and upper bound for MTTF confidence interval
    */
  def mttf(
    m: StochasticModel, 
    condition: Expr[Boolean], 
    nbSimulations: Int, 
    batchNumber: Int,
    confidence: Double = 0.95
  ): EitherResult[(Double, Double)] = {
    require(0 < confidence)
    require(confidence < 1)

    def observer(
      lastTime: Double, 
      state: StableState, 
      deadlock: Boolean, 
      timeState: Double, 
      totalTime: Double
    ): (Double, Boolean) = {
      (timeState, !eval(condition, state))
    }
    def simulationAggregator(resList: List[Double], lastTime: Double, totalTime: Double): List[Double] = {
      (totalTime - lastTime)::resList // substract time in failed state
    }
    val batchSize = nbSimulations / batchNumber
    val simus = 
      Vector.fill(batchNumber)(0.0).par.map{i => 
        stochasticSimulationBatch(m, observer, simulationAggregator, batchSize, 0.0, Nil)
      }
    val resSimus = simus.toVector.traverse(identity)
    resSimus.flatMap{ resListList =>
      Right(confidenceInterval(resListList.flatten, confidence, nbSimulations))
    }
  }

  // Not really availability as formal definition, the integral of it
  /** Stochastic simulation for average unavailability
    * Potential errors are printed and stop simulation 
    * @param m top-level component
    * @param condition failure condition
    * @param duration simulation duration
    * @param nbSimulations number of simulations
    * @param confidence confidence level (default 0.95)
    * @return lower and upper bound for unavailability confidence interval
    */
  def unavailability(
    m: StochasticModel, 
    condition: Expr[Boolean], 
    duration: Double, 
    nbSimulations: Int,
    confidence: Double = 0.95,
  ): EitherResult[(Double, Double)] = {
    require(0 < confidence)
    require(confidence < 1)

    def observer(
      time: Double, 
      state: StableState, 
      deadlock: Boolean, 
      timeState: Double, 
      totalTime: Double
    ): (Double, Boolean) = {
      val addWrong = if (eval(condition, state)) then 0.0 else timeState
      (time + addWrong, totalTime > duration)  
    }
    val simus = Vector.fill(nbSimulations)(0.0).par.map(i => stochasticSimulation(m, observer, 0.0))
    val resSimus: EitherResult[Vector[(Double, Double)]] = simus.toVector.traverse(identity)
    resSimus.flatMap{resList => 
      val unavail = resList.map((timeFailed, totalTime) => timeFailed / totalTime)
      Right(confidenceInterval(unavail, confidence, nbSimulations))
    }
  }

  /** Stochastic simulation for average unreliability
    * Potential errors are printed and stop simulation 
    * @param m top-level component
    * @param condition failure condition
    * @param duration simulation duration
    * @param nbSimulations number of simulations
    * @param batchNumber number of batches of simulations (executed in parallel)
    * @param confidence confidence level (default 0.95)
    * @return lower and upper bound for unreliability confidence interval
    */
  def unreliability(
    m: StochasticModel, 
    condition: Expr[Boolean], 
    duration: Double, 
    nbSimulations: Int, 
    batchNumber: Int,
    confidence: Double = 0.95,
  ): EitherResult[(Double,Double)] = {
    require(0 < confidence)
    require(confidence < 1)

    def observer(
      res: Double, 
      state: StableState, 
      deadlock: Boolean, 
      timeState: Double, 
      totalTime: Double
    ): (Double, Boolean) = {
      val condValue = eval(condition, state)
      (if condValue then 0.0 else 1.0, !condValue || totalTime > duration)  
    }
    def simulationAggregator(resList: List[Double], resSimu: Double, timeSimu: Double): List[Double] = {
      resSimu::resList
    }
    val batchSize = nbSimulations / batchNumber
    val resSimus = Vector.fill(batchNumber)(0.0).par.map{i => 
      stochasticSimulationBatch(m, observer, simulationAggregator, batchSize, 0.0, Nil)
    }.toVector.traverse(identity)
    resSimus.flatMap{resVect =>
      Right(confidenceInterval(resVect.flatten, confidence, nbSimulations))
    }
  }

}
