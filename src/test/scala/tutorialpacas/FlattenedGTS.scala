package tutorialpacas

import alpacas.Modelling.{*, given}
import alpacas.Analysis.*
import alpacas.*
import alpacas.transition.Transition
import alpacas.ast.Expr

def printTransition(t: Transition, model: CausalModel): String = {
  s"\\trans{${t.guard.toString(model)}}{${model.varNames.getEvent(t.ev.id)}}{${t.a.map(_.toString(model)).mkString("\\{", ", ~ ", "\\}")}}"
}

def printFlattened(c: Component): Unit = {
  compCheck(c).foreach{ model =>
    println(s"S &= &\\{ &${model.allStateVars.map{s => s"${s.toString(model)}: ~${s.init}"}.mkString(", ~ ")}~\\}\\\\")
    println(s"F &= &\\{ &${model.allFlowVars.map{f => f.toString(model)}.mkString(", ~ ")}~\\}\\\\")
    println(s"A_f &= &\\{ &${model.flowDef.map{as => s"${as.l.toString(model)} := ${as.r.toString(model)}"}.mkString(", \\\\ \n& & & ")}~\\}\\\\")
    println(s"T_R &= &\\{ &${model.allNonUrgentTransitions.withFilter{_.ev.policy == Policy.Restart}.map{printTransition(_, model)}.mkString(", \\\\ \n& & & ")}~\\}\\\\")
    println(s"T_M &= &\\{ &${model.allNonUrgentTransitions.withFilter{_.ev.policy == Policy.Memory}.map{printTransition(_, model)}.mkString(", \\\\ \n& & & ")}~\\}\\\\")
    println(s"T_U &= &\\{ &${model.allUrgentTransitions.map{printTransition(_, model)}.mkString(", \\\\ \n& & & ")}~\\}")

  }
}
