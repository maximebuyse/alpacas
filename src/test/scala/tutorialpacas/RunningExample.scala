package tutorialpacas.runningexample

import alpacas.Modelling.{*, given}
import alpacas.Analysis.*
import alpacas.*
import tutorialpacas.printFlattened
import java.lang.System
import scala.language.implicitConversions

import InputDistribution.*

enum Failure derives Lifted {
  case Ok
  case Fail
}

import Failure.*

given Ord[Failure] with {
  def lt(x: Failure, y: Failure): Boolean = x == Ok && y == Fail
}

class Battery extends Component {
  val power   = OutFlow[Failure]
  val state   = State[Failure](init = Ok)
  val failure = Event(Exponential(1E-5))
  val repair  = Event(Dirac(5), 1.0)
  assertions { power := state }
  transitions {
    When (failure) If state === Ok   Then { state := Fail }
    When (repair)  If state === Fail Then { state := Ok }
  }
}

class Engine extends Component {
  val thrust  = OutFlow[Failure]
  val power   = InFlow[Failure]
  val state   = State[Failure](init = Ok)
  val failure = Event(Exponential(1E-5), policy = Policy.Memory)
  val repair  = Event(Dirac(1))
  assertions { thrust := If (power === Ok && state === Ok) Then Ok Else Fail }
  transitions {
    When (failure) If (state === Ok && power === Ok)  Then { 
      state := Fail 
    }
    When (repair)  If state === Fail Then { state := Ok }
  }
}

type Batteries = Vector[Battery]
type Engines   = Vector[Engine]
type Wiring    = (Batteries, Engines) => Assertions

class Powertrain(wiring: Wiring, n: Int) extends Component {
  val batteries = Subs(n)(Battery()) 
  val engines   = Subs(n)(Engine())
  val observer  = OutFlow[Boolean]
  val ccf       = Event(Exponential(1E-7))
  assertions {
    wiring(batteries, engines)
    observer := engines.map(_.thrust === Ok).reduce(_&&_)
  }
  transitions { Sync(ccf) With { batteries.map(_.failure.hard).reduce(_&_) } }
}

def one2one(b: Batteries, e: Engines): Assertions =
  e.map(_.power) := b.map(_.power)

def one2all(b: Batteries, e: Engines): Assertions =
  for (eng <- e) eng.power := b.map(_.power).reduce(_ min _)

val powertain121    = Powertrain(one2one, 2)
val powertrain12all = Powertrain(one2all, 2)

def time[T](c: => T): (T, Long) = {
  val begin = System.currentTimeMillis()
  val res = c
  val end = System.currentTimeMillis()
  (res, end - begin)
}

object RunningExample extends App {
  val (model, modeltime) = time(stochasticCheck(powertrain12all))
  println(s"Flattening time: $modeltime")
  for {
    model <- model
    (mcs, timeMCS) = time(minimalCutSets(model, powertrain12all.observer, 5))
    mcs <- mcs
  }{
    val (bdd, timeBDD) = time(structureFctBDD(model, mcs))
    mcs.foreach(println(_))
    println(s"MCS time: $timeMCS")
    println(s"BDD time: $timeBDD")

    val numberOfSimus = 100000

    for {
      // first two runs are dry-runs added to let the JVM JIT our model
      t <- 1E2::1E1::1E2::1E3::1E4::1E5::Nil
    }{
      println(s"\nDuration: $t")
      val (urmcs, mcstime) = time(unreliabilityBDD(model, bdd, t, mcs))
      println(s"MCS unreliability: $urmcs ($mcstime ms)")
      val (Right((urmin, urmax)), cpuTime) = time(unreliability(model, powertrain12all.observer, t, numberOfSimus, 8))
      val ur = (urmin + urmax) / 2
      val uncertainty = (urmax - urmin) / 2
      println(s"Simu unreliability: $ur +- $uncertainty ($cpuTime ms)") 
    }
  }
}

object FlattenedExample extends App {
  printFlattened(powertrain12all)
}

object InteractiveSimulation extends App {
  for (model <- compCheck(powertrain12all)) interactiveSimulationB(model)
}
