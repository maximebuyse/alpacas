package tutorialpacas.dse

import scala.language.implicitConversions
import alpacas.Modelling.{*, given}
import alpacas.Analysis.*
import alpacas.*

object ExplorationExample extends App {

  /*
  class GenericPowertrain(
    val engineParameters: EngineParameters,
    val lambdaSensor: Double,
    val lambdaCpu: Double,
    val nofCpus: Int,
    val optimisticSensor: Boolean,
    val oppositeEngine: Int => Int,
    val wiring: Wiring,
    val seg: Boolean,
  ) extends Component
  */
  val lambdaCpu = 1E-10
  val nofCpus = 2
  val nbSimus = 100000
  val lambdaBattery = 1E-5

  object ThrustRealloc {
    def apply(
      engineParams: EngParams,
      lambdaSensor: Double,
      optimisticSensor: Boolean,
      wiringFunc: Wiring,
    ): GenericPowertrain = {
      GenericPowertrain(
        EngineParameters(
          engineParams.nEng, 
          engineParams.lam0, 
          engineParams.lam1, 
          engineParams.lam2,
          lambdaBattery
        ),
        lambdaSensor,
        lambdaCpu,
        nofCpus,
        optimisticSensor,
        opposite(engineParams.nEng),
        wiringFunc,
        false,
      )
    }
  }


  case class EngParams(nEng: Int, lam0: Double, lam1: Double, lam2: Double)

  class ThrustRealloc(
    val engineParams: EngParams,
    val lambdaSensor: Double,
    val optimisticSensor: Boolean,
    val wiring: Wiring,
  ) extends Component { /* Model declaration */ }

  val systems = for {
    eps     <- List(
      EngParams(6, 1E-5, 1E-4, 2E-4), 
      EngParams(8, 2E-5, 1E-4, 2E-4), 
      EngParams(10, 2.5E-5, 1E-4, 2E-4)
    )
    lamSens <- List(1E-5, 1E-10)
    optSens <- List(true, false)
    wiring  <- List(stdWiring(eps.nEng), segWiring(eps.nEng))
  } yield ThrustRealloc(eps, lamSens, optSens, wiring)

  var minUR = Double.PositiveInfinity
  var bestSystem: Option[GenericPowertrain] = None

  for {
    system <- systems
    model  <- stochasticCheck(system)
    mcs    <- minimalCutSets(model, system.observer.isOk, 3)
    urRes  <- unreliability(model, system.observer.isOk, 1E3, nbSimus, 8)
    (_, urmax) = urRes
  } {
    if (mcs.forall(_.events.size > 1) && urmax < minUR) then
      bestSystem = Some(system)
      minUR = urmax
  }
}
