package alpacas.step

import scala.collection.mutable.ArrayBuffer
import org.scalacheck.*
import org.scalacheck.Prop.forAll
import Arbitrary.arbitrary
import Gen.*

import alpacas.ast.*
import alpacas.flattening.*
import alpacas.stateImpl.*
import alpacas.exprEval.*
import alpacas.frontend.*
import alpacas.*
import alpacas.Modelling.{*, given}
import alpacas.Analysis.*

class FireableModel extends Component {
  val sInt1 = State[Int](0)
  val sInt2 = State[Int](1)
  val sBool = State[Boolean](true)

  val e1 = Event()
  val e2 = Event()
}

class HiddenModel extends Component {
  val e = Event()
  transitions{
    When(e) Then {}
  }
}

val c = FireableModel()

val genLeafBool: Gen[Expr[Boolean]] = oneOf(const(c.sBool), arbitrary[Boolean].flatMap(Expr.Const(_)))

val genLeafInt: Gen[Expr[Int]] = oneOf(oneOf(c.sInt1, c.sInt2), arbitrary[Int].flatMap(Expr.Const(_)))

val exprGen =  ExprGenerator(genLeafBool, genLeafInt, false)

given Arbitrary[Expr[Boolean]] = Arbitrary(exprGen.genExprBool)

/** Fireables transitions update property based testing :
  * We check that the enumeration of fireable transitions corresponds to those for which 
  * the guard evaluates to true. We verify that hidden transitions are never fireable. */
object ExprEvaluationSpec extends Properties("Update fireable") {
  property("Update Fireable") = forAll{ (g1: Expr[Boolean], g2: Expr[Boolean], mutable: Boolean) =>
    given Transitions = c.trans.transitions
    When(c.e1) If(g1) Then {}
    When(c.e2) If(g2) Then {}

    compCheck(c) match
      case Right(cm) =>
        val state = 
          if mutable then
            mutableInitialState(cm)
          else
            immutableInitialState(cm)

        val fireablesRef = for{
          (g, id) <- Set((g1, c.e1.id), (g2, c.e2.id))
          if(eval(g, state))
        } yield id

        val fireables = EventSet(cm)
        updateFireable(cm, state, fireables)

        val fireablesTest = fireables.toVector.toSet

        c.trans.transitions -= c.trans.transitions.last
        c.trans.transitions -= c.trans.transitions.last

        fireablesTest == fireablesRef
      case Left(err) =>
        false
  }

  property("hidden not fireable") = forAll{(h: Boolean, mutable: Boolean) =>
    val m = HiddenModel()
    if h then
      m.e.hide
    compCheck(m) match
      case Right(cm) =>
        val state = 
          if mutable then
            mutableInitialState(cm)
          else
            immutableInitialState(cm)

        val fireables = EventSet(cm)
        updateFireable(cm, state, fireables)

        if h then
          fireables.size == 0
        else 
          fireables.size == 1 && fireables.contains(m.e.id)

      case Left(err) => false
  }
}