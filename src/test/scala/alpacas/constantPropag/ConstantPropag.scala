package alpacas.constantPropag

import org.scalacheck.*
import Gen.*
import Arbitrary.arbitrary
import org.scalacheck.Prop.forAll
import scala.collection.mutable.ArrayBuffer

import alpacas.ast.*
import alpacas.ast.StateIds.*
import alpacas.ast.FlowIds.*
import alpacas.assertion.*
import alpacas.exprEval.*
import alpacas.stateImpl.*
import alpacas.Analysis.*
import alpacas.*
import alpacas.Modelling.{*, given}

/** Property-based testing on generated expressions: 
  * we check that the evaluation of the raw expression gives the same result as 
  * the evaluation of the simplified expression.
  */
object CstPropagSpec extends Properties("Constant propagation") {
  val sBool = Expr.Svar(StateId(), true, "sbool", 12).asInstanceOf[Expr.Svar[Boolean]]

  val fBool = Expr.Fvar[Boolean](FlowId(), "fbool", 15).asInstanceOf[Expr.Fvar[Boolean]]

  val genLeafBool: Gen[Expr[Boolean]] = frequency((1, sBool), (1, fBool), (10, arbitrary[Boolean].flatMap(Expr.Const(_))))


  val sInt = Expr.Svar(StateId(), 1, "sInt", 50).asInstanceOf[Expr.Svar[Int]]

  val fInt = Expr.Fvar[Int](FlowId(), "fInt", 53).asInstanceOf[Expr.Fvar[Int]]

  val genLeafInt: Gen[Expr[Int]] = 
    frequency(
      (1, sInt), 
      (1, fInt), 
      (10, (arbitrary[Int] suchThat (_ !=0)).flatMap(Expr.Const(_)))
    )

  val genExpr = ExprGenerator(genLeafBool, genLeafInt, false)

  implicit lazy val arbExprBool: Arbitrary[Expr[Boolean]] = Arbitrary(genExpr.genExprBool)
  implicit lazy val arbExprInt: Arbitrary[Expr[Int]] = Arbitrary(genExpr.genExprInt)

  class EmptyComponent extends Component


  property("ConstantPropag") = forAll {(
      fBoolDef: Expr[Boolean], 
      fIntDef: Expr[Int]
    ) =>

      val comp = EmptyComponent()

      comp.svar ++= Iterable(
        sBool, 
        sInt, 
      )

      comp.fvar.out ++= Iterable(
        fBool, 
        fInt, 
      )

      given ArrayBuffer[FlowAssertion[_]] = comp.assert.assertions
      fBool := fBoolDef
      fInt := fIntDef
      Prop.classify(fBoolDef != propagate(fBoolDef), "Propag Different"){
        compCheck(comp) match
          case Right(cm) => 
            val state = mutableInitialState(cm)
            (eval(fBoolDef, state) == eval(propagate(fBoolDef), state)) && 
            (eval(fIntDef, state) == eval(propagate(fIntDef), state))
          case Left(err) =>
            true
      }
  }
}